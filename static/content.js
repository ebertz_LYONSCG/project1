function parseCookies() {
	if (document.cookie == "") return null;
	var cookieStr = document.cookie.split(";");
	var cookieObj = {};
	for(index in cookieStr) {
		var key, value;
		[key, value] = cookieStr[index].trim().split("=",2);
		cookieObj[key] = value;
	}
	return cookieObj;
}
function buildCookieTable() {
	var cookies = parseCookies();
	console.log(cookies);
	if (cookies == null) return;
	var table = document.getElementById("cookieJar");
	table.style.display = "table";
	var row = document.createElement("tr");
	row.innerHTML = "<tr><th>Name</th><th>Value</th><th>Delete</th></tr>";
	table.appendChild(row);
	var deleteButton = "<button onclick='deleteCookie(this)'> X </button>"
	for (key in cookies) {
		row = document.createElement("tr");
		row.innerHTML = "<tr><td class='key'>"+key+"</td><td>"+cookies[key]+"</td><td>"+deleteButton+"</td></tr>";
		table.appendChild(row);
	}
}
function deleteCookie(o) {
		var p=o.parentNode.parentNode;
		cookieName = p.getElementsByClassName('key')[0].innerHTML;
		document.cookie = cookieName + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
	 	p.parentNode.removeChild(p);
	 	if (document.cookie == "") document.getElementById("cookieJar").style.display = "none";
}

document.addEventListener("DOMContentLoaded", function() {
	buildCookieTable();


});