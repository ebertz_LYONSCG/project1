This application implements a simple Node.js server that renders a single page in the browser.

Users can add cookies to the browser using the input forms.
Cookies will display in a table, and can be deleted by clicking the 'X' button in the corresponding row.
AJAX is used to update the cookie table asychronously when a cookie is added.

Users can all submit a GET or POST request containing the data entered into the get and post forms.

Request header data is displayed in the table on the bottom.

Dependencies:
	Node.js
	Express
	ejs

Running the application
	To install dependencies using npm, type 'npm install'
	type 'node server.js' to start the local server on port 8888