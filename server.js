var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var url = require('url');
//var ejs = require('ejs');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
app.set('view engine', 'ejs');

app.use(express.static('static'));


app.get('/', (req, res) => {
	console.log(req.headers);
	var get_data = "";
	var post_data= "";

	res.render(__dirname +'/static/home.ejs', {headers: req.headers, get_data: get_data, post_data: post_data});
});


app.post('/cookies', (req, res) => {
	if (req.body.name != "" && req.body.value != "")
		res.setHeader('Set-Cookie', req.body.name + "=" + req.body.value);
	res.redirect('/');
});

app.post('/post', (req,res) => {
	var get_data = req.query.get_data;
	var post_data = req.body.post_data;

	res.render(__dirname +'/static/home.ejs', {headers: req.headers, get_data: get_data, post_data: post_data});

});

app.get('/get', (req,res) => {
	var get_data = req.query.get_data;
	var post_data = req.body.post_data;
	res.render(__dirname +'/static/home.ejs', {headers: req.headers, get_data: get_data, post_data: post_data});

});

app.get('/about', (req,res) => {
	res.render(__dirname +'/static/about.ejs');
});
app.listen(8888, () => console.log("app running"));